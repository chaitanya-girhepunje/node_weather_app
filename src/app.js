const path = require("path");
const express = require("express");
const hbs = require("hbs");
const geocode = require("./utils/geocode");
const forecast = require("./utils/forecast");

const app = express();

// Define paths for Express config
const publicDirectoryPath = path.join(__dirname, "../public");
const viewsPath = path.join(__dirname, "../templates/views");
const partialsPath = path.join(__dirname, "../templates/partials");

// setup handlebars engine and views location
app.set("view engine", "hbs");
app.set("views", viewsPath);
hbs.registerPartials(partialsPath);

// 1 @ setup  static directory to serve
app.use(express.static(publicDirectoryPath));

app.get("", (req, res) => {
  res.render("index", {
    title: "Index Weather App",
    name: "Index Andrew Mead",
    footer: "Chaitanya-Index",
  });
});

app.get("/about", (req, res) => {
  res.render("about", {
    title: "about Weather App",
    name: "about Andrew Mead",
    footer: "Chaitanya-about",
  });
});

app.get("/help", (req, res) => {
  res.render("help", {
    helptext: "this is some helpful text",
    title: "Help Weather App",
    name: "Help Andrew Mead",
    footer: "Chaitanya-Help",
  });
});

app.get("/weather", (req, res) => {
  if (!req.query.address) {
    return res.send({
      error: "you must provide a address",
    });
  }

  geocode(
    req.query.address,
    (error, { latitude, longitude, location } = {}) => {
      if (error) {
        return res.send({ error });
      }

      forecast(latitude, longitude, (error, forecastData) => {
        if (error) {
          return res.send({ error });
        }

        res.send({
          forecast: forecastData,
          location,
          address: req.query.address,
        });
      });
    }
  );
});

app.get("/products", (req, res) => {
  if (!req.query.search) {
    return res.send({
      error: "You must provide a search term",
    });
  }
  console.log(req.query.search);
  res.send({
    products: [],
  });
});

app.get("/help/*", (req, res) => {
  res.render("404", {
    title: "404",
    name: "404 Andrew Mead",
    errorMessage: "Help content not found",
  });
});

app.get("*", (req, res) => {
  res.render("404", {
    title: "404",
    name: "404 Andrew Mead",
    errorMessage: "Page Not Found",
  });
});

app.listen(3000, "0.0.0.0", () => {
  console.log("Server is up on port 3000");
});
